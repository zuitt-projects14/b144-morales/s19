console.log("Hello World");

let num = 2;
const getCube = num ** 3;
 console.log(`The cube of ${num} is ${getCube}`);


const address = [
				258,
				"Washington Ave",
				"NW",
				"California",
				90011,	
				];

const [street, avenue, city, state, zipCode ] = address;

console.log(`I live at ${street} ${avenue} ${city}, ${state} ${zipCode}`);


const animal = {
	name: "Lolong",
	type: "Saltwater Crocodile",
	weight: 1075,
	measurement: "20 ft 3 in."
} 

const {name, type, weight, measurement} = animal;



console.log(`${name} was a ${type}. He weighted at ${weight} kgs with a measurement of ${measurement}`);


const numbers = [1, 2, 3, 4, 5,];



numbers.forEach(num => console.log(`${num}`));

const reduceNumber = numbers.reduce((a, b) => a + b);

console.log(reduceNumber);



class Dog {
	constructor (name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new Dog("Frankie", 5, "Miniature Dachshund")
console.log(myDog);












