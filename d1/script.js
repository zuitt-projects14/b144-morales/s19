/*console.log("Hello World");*/

//Javascript ES6

//1. Exponents operator
//2. pre version

const firstNum = Math.pow(8, 2);
console.log(firstNum);

//ES6
const secondNum = 8 ** 2;
console.log(secondNum);


//2. TEMPLATE Literals without using the concatenation operator (+)
//
 /*`` "" ''*/
let name = "John"
 //-template literal string4
 //Uses single quotes (" ")
 let message = "Hello " + name + '! Welcome to programming!';
 console.log("Message without template literals: " + message)

 message = `Hello ${name}! Welcome to programming!`;

 console.log(`Message with template literals: ${message}`);

//Multi-line Using template literals
const anotherMessage = `${name} attended a math competition. He won it by solving the problem 8 ** 2 with solution of ${secondNum}.`


console.log(anotherMessage);

//Template literals allow us to write strings with embedded JS expression (${})



const interestRate = .1;
const principal = 1000;



console.log(`The interest on your savings account is ${principal * interestRate}`)


//3. Array Destructuring
//Allows us to unpack elements in arrays into distinct variables
//allows us to name array elements with variables instead of using index number. Helps with code readability.
//Syntax: let/const [variableName, variableName] = array

const fullName = ["Juan", "Dela", "Cruz"];
//pre-array destructuring
console.log(fullName[0])
console.log(fullName[1])
console.log(fullName[2])


console.log(`hello ${fullName[0]} ${fullName[1]} ${fullName[2]}`);


//Array Destructuring
const [firstName, middleName, lastName] = fullName;

console.log(firstName);
console.log(middleName);
console.log(lastName);

//expressions are any valid unit of code that resolves to a value
console.log(`Hello ${firstName} ${middleName} ${lastName}`);

//4. Object destructuring
//Allows us to unpack elements in obect into distinct variables

const person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
}

//pre array destructuring
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again`);

//Object destructuring
//It shortens the syntax from accessing properties from objects
//Syntax: let/const {propertyName, propertyName} = object

const {givenName, maidenName, familyName} = person;

console.log(givenName);

console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you again`);
function getFullName({ givenName, maidenName, familyName }){
	console.log(`${givenName} ${maidenName} ${familyName}`)
}

getFullName(person);

//5. Arrow Functions
//Normal Function

function pritFullName(fName, mName, lName){
	console.log(`${fName} ${mName} ${lastName}`)
}

//function declaration
//function name
//parameters = placeholder or the name of the argument/s to be passed to the function
//statement = function body
//invoke/call back function



//ES6 arrow function
//Compact alternative syntax to traditional function
//Useful for code snippets where creating function will not be reused in any other portion of the code
//as anonymous function
//Adheres to the "FRY principle" (Don't repeat yourself) where there's no longer need to create a function and think of a name of functions that will only be used in certain snippets
const variableName = () => {
	console.log("Hello World")
}

const printFullName = (fName, mName, lName) => {
	console.log(`${fName} ${mName} ${lastName}`)
}

printFullName("John", "D", "Smith")

//Arrow Function w/ loops
//pre-arrow function

const students = ["John", "Jane", "Joe"];

students.forEach(
	function(student){
		console.log(`${student} is a student`)
	}
	)

students.forEach((student) => console.log(`${student} is a student`));

//6. (Arrow function) Implicit return statement/ 

//There are instances where you can omit the return statement - this works because even without the return statement javacsript implicitly add it for the result of the function
// Pre-arrow function 

/*const add = (x, y) => {
	return x + y;
}

let total = add(1, 2);
console.log(total)*/
//can omit return
//Arrow Function
const add = (x, y) => x + y

let total = add(1,2);
console.log(total);

/*let filterFriends = friends.filter(friend => friend.length === 4);*/
//if isa lang return or parameter pwede na i omit si ().

//7. (Arrow Function) Default function argument value
//Provides a default argument value if none is provided when the function is invoked.

const great = (name = "User") => {
	return `Good morning! ${name}`
}
//default value na user
console.log(great());


//8. creating Class
//pag class always may kasamang constructor

/*
Syntax: 

class className {
	constructor(objectPropertyA, objectPropertyB){
		this.objectPropertyA = objectPropertyA;
		this.objectPropertyB = objectPropertyB
	}
}

//The constructor is a special method for creating or initializing an object for that class.
//The This keyword refers to the properrties of an object created from the class.
*/

class Car {
	constructor(brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

const myCar = new Car();
console.log(myCar);


//Value of properties may be assigned after creation or instantiation of an object
myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = 2021;

console.log(myCar)
//Creating instantiating a new object from the car class with initialized values.

const myNewCar = new Car("Toyota", "Vios", 2021)


//9. Ternary Operator
//Conditional operator
//It has three operands
//condition, followed by question mark ?. then and expression.

if (condition == 0){
	return true

}else {
	false
}

//Ternary operator
(condition == 0) ? true : false








































